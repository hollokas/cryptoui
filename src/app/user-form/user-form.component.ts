import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  userForm = this.formBuilder.group({
    username: ['', Validators.required],
    quotes: this.formBuilder.array([
      this.formBuilder.control('')
    ])
  });

  constructor(private data: UsersService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
  }

  onSubmit() {

    this.data.addUser(this.userForm.value).subscribe(
      response => this.router.navigateByUrl('/users')
    );

    console.log(this.userForm.value);
  }

  get quotes() {
    return this.userForm.get('quotes') as FormArray;
  }

  addQuote() {
    this.quotes.push(this.formBuilder.control(''));
  }
}
