import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Crypto {
  currencyCode: string;
  currencyName: string;
}

@Injectable({
  providedIn: 'root'
})
export class CryptosService {

  constructor(private http: HttpClient) { }

  getAllCryptoCodes() {
      return this.http.get('http://localhost:8030/api/cryptoservice/cryptos/fetchAllCryptoCodes');
  }
}
