import { Component, OnInit } from '@angular/core';
import { Crypto, CryptosService } from './cryptos.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cryptos',
  templateUrl: './cryptos.component.html',
  styleUrls: ['./cryptos.component.scss']
})

export class CryptosComponent implements OnInit {

  public cryptos$: Crypto[];

  constructor(private data: CryptosService) { }

  ngOnInit() {
    this.data.getAllCryptoCodes()
      .subscribe(
        result => {
          this.cryptos$ = result as Crypto[];
        }
      );
  }

}
