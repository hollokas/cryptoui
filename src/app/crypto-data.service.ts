import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { CryptoDetails } from './crypto-details';

@Injectable({
  providedIn: 'root'
})
export class CryptoDataService {

  private cryptoDetailsSource = new BehaviorSubject<CryptoDetails>(new CryptoDetails());
  currentCryptoDetails = this.cryptoDetailsSource.asObservable();

  constructor() { }

  changeCurrentCryptoDetails(crypto: CryptoDetails) {
    this.cryptoDetailsSource.next(crypto);
  }
}
