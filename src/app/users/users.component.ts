import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users$: String[];

  constructor(private data: UsersService) { }

  ngOnInit() {
    this.data.getAllUsers()
      .subscribe(
        result => {
          this.users$ = result as String[];
        }
      );
  }

  deleteUser(user: String) {

    this.users$ = this.users$.filter(usr => usr !== user);

    this.data.deleteUser(user).subscribe();

  }

}
