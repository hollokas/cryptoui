import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCryptosComponent } from './user-cryptos/user-cryptos.component';
import { UserFormComponent } from './user-form/user-form.component';
import { CryptosComponent } from './cryptos/cryptos.component';
import { UsersComponent } from './users/users.component';
import { CryptoPlotComponent } from './crypto-plot/crypto-plot.component';

const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: 'users', component: UsersComponent },
  { path: 'users/:name', component: UserCryptosComponent },
  { path: 'user-form', component: UserFormComponent },
  { path: 'cryptos', component: CryptosComponent },
  { path: 'crypto-plot/:crypto', component: CryptoPlotComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
