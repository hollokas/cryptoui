import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UserCrypto } from './user-crypto';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get('http://localhost:8030/api/cryptoservice/cryptos/fetchAllUsers');
  }

  addUser(formData: any) {
    return this.http.post('http://localhost:8030/api/cryptoservice/cryptos/addQuote', formData, httpOptions);
  }

  getUserCryptos(username: String) {
    console.log(username);
    return this.http.get('http://localhost:8030/api/cryptoservice/cryptos/' + username);
  }

  deleteUser(username: String) {
    return this.http.delete('http://localhost:8030/api/cryptoservice/cryptos/deleteUserQuotes/' + username);
  }

  deleteQuote(userCrypto: UserCrypto) {
    console.log('HERE!');
    console.log(userCrypto);
    return this.http.post('http://localhost:8030/api/cryptoservice/cryptos/deleteQuote', userCrypto, httpOptions);
  }
}
