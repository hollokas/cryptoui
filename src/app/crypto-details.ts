import { Currencies } from './currencies';

export class CryptoDetails {
    constructor(
        public currencyName?: String,
        public currencyCode?: String,
        public lastRefreshed?: String,
        public lastPriceEUR?: Number,
        public lastPriceUSD?: Number,
        public currencyChanges?: Map<String, Currencies>
    ) {}
}
