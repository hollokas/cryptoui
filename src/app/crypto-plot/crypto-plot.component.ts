import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { CryptoDetails } from '../crypto-details';
import { CryptoDataService } from '../crypto-data.service';

import { ActivatedRoute } from '@angular/router';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-crypto-plot',
  templateUrl: './crypto-plot.component.html',
  styleUrls: ['./crypto-plot.component.scss']
})
export class CryptoPlotComponent implements OnInit {

  @ViewChild('lineChart') private chartRef: ElementRef;
  chart: any;

  currentCryptoDetails: CryptoDetails;
  chartLabels: string[] = [];
  chartValues: number[] = [];
  currencyCode: string;

  constructor(private activatedRoute: ActivatedRoute, private data: CryptoDataService) { }

  ngOnInit() {

    // Get data from details component
    this.data.currentCryptoDetails.subscribe(
      result => this.currentCryptoDetails = result
    );

    console.log(this.currentCryptoDetails);

    // Create array for selected labels
    const tempLabelArr = Array.from(Object.keys(this.currentCryptoDetails.currencyChanges));

    for (let i = 0; i < 144; i++) {
      this.chartLabels.push(tempLabelArr[i]);
    }

    // Create array for data
    Object.keys(this.currentCryptoDetails.currencyChanges).forEach(
      key => this.chartValues.push(this.currentCryptoDetails.currencyChanges[key].EUR)
    );

    this.chartValues = this.chartValues.slice(0, 144);


    this.currencyCode = this.activatedRoute.snapshot.paramMap.get('crypto');
    console.log(this.currencyCode);

    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'line',
      data: {
        labels: this.chartLabels.reverse(), // your labels array
        // labels: tempLabelArr.reverse(), // your labels array

        datasets: [
          {
            data: this.chartValues.reverse(), // your data array
            borderColor: '#00AEFF',
            fill: false
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }
}
