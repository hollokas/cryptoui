import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CryptoPlotComponent } from './crypto-plot.component';

describe('CryptoPlotComponent', () => {
  let component: CryptoPlotComponent;
  let fixture: ComponentFixture<CryptoPlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CryptoPlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
