import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UserCryptosComponent } from './user-cryptos/user-cryptos.component';
import { CryptosComponent } from './cryptos/cryptos.component';
import { UsersComponent } from './users/users.component';
import { UserFormComponent } from './user-form/user-form.component';
import { CryptoPlotComponent } from './crypto-plot/crypto-plot.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    UserCryptosComponent,
    CryptosComponent,
    UsersComponent,
    UserFormComponent,
    CryptoPlotComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
