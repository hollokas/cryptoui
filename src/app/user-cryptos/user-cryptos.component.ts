import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { UsersService } from '../users.service';
import { CryptoDataService } from '../crypto-data.service';
import { CryptoDetails } from '../crypto-details';
import { UserCrypto } from '../user-crypto';

@Component({
  selector: 'app-user-cryptos',
  templateUrl: './user-cryptos.component.html',
  styleUrls: ['./user-cryptos.component.scss']
})
export class UserCryptosComponent implements OnInit {

  public cryptoDetails$: CryptoDetails[];
  public currentCryptoDetails: CryptoDetails;
  public username: String;

  constructor(
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private location: Location,
              private userService: UsersService,
              private cryptoDataService: CryptoDataService,
              private spinnerService: Ng4LoadingSpinnerService
            ) { }

  ngOnInit() {
    this.getUserCryptos();
  }

  getUserCryptos() {
    this.spinnerService.show();
    this.username = this.activatedRoute.snapshot.paramMap.get('name');
    this.userService.getUserCryptos(this.username).subscribe( result => {
      this.spinnerService.hide();
      this.cryptoDetails$ = result as CryptoDetails[];
    });
  }

  setCurrentCryptoDetails(currentCryptoDetails: CryptoDetails) {
    this.currentCryptoDetails = currentCryptoDetails;
    this.sendCurrentCrypto();
    this.router.navigateByUrl('crypto-plot/' + currentCryptoDetails.currencyCode);
  }

  sendCurrentCrypto() {
    this.cryptoDataService.changeCurrentCryptoDetails(this.currentCryptoDetails);
  }

  deleteCrypto(cryptoDetail: CryptoDetails) {
    const userCrypto: UserCrypto = new UserCrypto(this.username, [cryptoDetail.currencyCode]);

    this.userService.deleteQuote(userCrypto).subscribe(
      result => {
        this.cryptoDetails$.length === 1 ?
          this.router.navigateByUrl('users') :
          this.cryptoDetails$ = this.cryptoDetails$.filter(loc => loc.currencyCode !== userCrypto.quotes[0]);

      }
    );
  }

}
